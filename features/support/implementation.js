'use strict';

/*
This is an actual implementation that will interface with the application / platform / architecture under test
 */
const r2 = require("r2");

const URL_UNDER_TEST_CORRECT = "https://dog.ceo/api/breed/hound/list";
const URL_UNDER_TEST_INCORRECT = "https://dog.ceo/api/breed/notcorrect/list";

var Implementation = function () {

};

var TRIANGLE_URL = undefined;

Implementation.prototype.prepareForTriangleCalculation = () => {
    TRIANGLE_URL = process.env.TRIANGLE_URL;
    if(TRIANGLE_URL == undefined){
        throw new Error("Environment variable TRIANGLE_URL is not defined");
    }
    console.log("Prepare for triangle calculation")
};

Implementation.prototype.doCalculationRequest = async (numberOfEqualSide) => {

    var triangleType = undefined;
    var request = undefined;
    if (numberOfEqualSide == 1) {
        request = {"side1": 1, "side2": 2, "side3": 3};
    } else if (numberOfEqualSide == 2) {
        request = {"side1": 1, "side2": 1, "side3": 3};
    } else if (numberOfEqualSide == 3) {
        request = {"side1": 1, "side2": 1, "side3": 1};
    } else {
        throw new Error("Number of equal sides should be a number between 1 and 3")
    }
    console.log("Request calculation of triangle type");
    const triangleResponse = await getLocation(TRIANGLE_URL, request);
    if(triangleResponse.triangleType != undefined){
        triangleType = triangleResponse.triangleType;
        console.log(triangleType);
    }else{
        throw new Error("Triangle type cannot be determined");
    }
    return triangleType;
};


const getLocation = async (url, data) => {
    try {
        const response = await r2.post(url, {json: data}).json
        return response;
    } catch (error) {
        console.log(error);
    }
};


module.exports = new Implementation();