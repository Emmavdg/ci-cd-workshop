const RADIX = 10;

exports.handler = function (event, context, callback) {

    var side1 = event.side1,
        side2 = event.side2,
        side3 = event.side3;

    var error = null;

    if (validateSide(side1) && validateSide(side2) && validateSide(side3)) {

        side1 = parseInt(side1, RADIX);
        side2 = parseInt(side2, RADIX);
        side3 = parseInt(side3, RADIX);

        var triangleType = null;

        if (side1 === side2 && side1 === side3) {
            triangleType = {"triangleType": "equilateral"};
        } else if (side1 === side2 || side1 === side3 || side2 === side3) {
            triangleType = {"triangleType": "isosceles"};
        } else {
            triangleType = {"triangleType": "scalene"};
        }

    } else {
        error = new Error("invalid input");
    }

    callback(error, triangleType);
};


/**
 * Validate a side
 *
 * @param side
 * @returns {boolean}
 */
function validateSide(side) {

    var valid = true;
    try {
        valid &= side != undefined;
        side = parseInt(side, RADIX);
        valid &= side > 0;
    } catch (err) {
        valid = false;
    }
    return valid;
}