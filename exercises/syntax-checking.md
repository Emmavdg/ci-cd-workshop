# Syntax checking

Syntax checking is a very fast way to validate whether the set syntax rules for a team/organisation are met. 
These can vary from language-wide best practices or pitfalls, to purely subjective choices like "tabs vs spaces". 

Roughly said, this is a fast grammar check for your code that can help get some painful, easily overlooked errors out of the process.
This can also prevent discussions on certain styles, by simply selecting one and enforcing it. 

## Goals

Have a mechanism in place to fail-fast when our syntax is not correct, or the code does not adhere to a certain style.

## Approach

Validating syntax can be done by linting frameworks and/or checkstyle setups. In case of our NodeJS (JavaScript) application, we use [ESLint](https://eslint.org/). 

ESLint can be used together with our build tool / package manager for NodeJS, [`npm`](https://www.npmjs.com/), for easier use. Click ESLint or NPM if you want to learn more!

## Exercise

We're going to have the linting done by the ESLint framework, used in combination with the npm build 
tool/package manager. In the [package.json](../package.json) is a step **lint** defined. This will call the eslint 
framework that validates the JavaScript code style.

### Step 1: disable sample stage and job

The sample stage and job are an example for a minimal CI/CD pipeline setup. The stage can be disabled by adding a pound 
before the stage name, by replacing the contents of [.gitlab-ci.yml](../.gitlab-ci.yml) file, or by simply removing the stages altogether. Since we use a version control system, we could always retrieve and re-enable these stages later.

Go to CI/CD --> Editor in the sidebar on the left (preferably . Here you'll see the contents of `.gitlab-ci.yml` and you can disable the `sample` stage with your preferred way of choice: 
```yaml
# Default docker image
image: node:10

# GitLab CI settings
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
  - node_modules/

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2

before_script:
  - npm install

# Stages

stages:
# Disable
#  - sample


# Jobs

# Disable with .
.sample:
  stage: sample
  script:
   - echo 'sample'
```

### Step 2: Create a linting step and stage

Create a linting step and stage that will now be the first step in our delivery pipeline. Add the QA stage and linting job 
to [.gitlab-ci.yml](../.gitlab-ci.yml). Hint: Use the aforementioned editor for this! 

```yaml
stages:
  - qa

linting:
  stage: qa
  script:
   - npm run lint

```

The stage **qa** is a new logical divider of steps within the continuous delivery pipeline. The job **linting** contains 
the actual command that starts the linting, in this case via `npm`.

After this, enter a relevant *commit message* (essentially describe the result of your change, like "enabling linting"), and finish your change by clicking `Commit Changes`.

### Step 3: Verify results
To see what your change just did, go to the sidebar,  CI/CD --> Pipelines to get an overview of pipelines & jobs currently running for your project. 
Your changes will be running in the background as we speak! If all is well, you should end up with green buttons all around. 

You've just enabled an automated grammar & style check for every change to come, ensuring that we don't have to find out about this
the hard way (e.g. once we're trying to build, or worse yet, deploy our application).

Congrats! Time to move on to exercise 4: [Building and package validation](exercises/code-build.md). 

