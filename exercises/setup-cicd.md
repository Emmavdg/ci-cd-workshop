# Setting up a CI/CD pipeline on GitLab

This project comes with a simple setup; The "application" we have here itself is a small NodeJS(JavaScript) calculator, 
that based on the lengths of a triangle can tell you what kind of triangle it is.

More importantly for this workshop, we have a basic CI/CD pipeline in a file called `.gitlab-ci.yml`. 
Effectively, this tells Gitlab that every change made to this project (that is pushed to this remote, or main repository)
will trigger execution of the pipeline defined there. 

This is one of the main concepts of CI(/CD): Every change should result in an automated build & testing process. 

We're using Gitlab CI for this particular demo. For more information on configuring GitLab CI can be found [here](https://docs.gitlab.com/ee/ci/yaml/).

## Setup GitLab CI/CD variables

To connect to the actual application a variable must be set in the environment of the pipeline. 
Ask your workshop lead for the value (if possible) and follow these steps for setting the variables:

1. Go to the pipeline configuration page; use this URL (don't forget to adjust it): https://gitlab.com/**gitlab-account**/ci-cd-workshop/-/settings/ci_cd 
OR go to Settings --> CI/CD in the lower left corner. 
2. Go to the Variables section and click on **Expand**, this will open the input menu. Warning: The picture below says "Secret Variables", but it's just called "Variables" now! ![Secret variables](images/secrets.png)
3. Create a new secret variable called **TRIANGLE_URL**, with a value provided in the workshop. ![Secret input](images/secret-input.png)
4. Add the variable by clicking **Add variable**.

## .gitlab-ci.yml

During the exercises we will extend the **.gitlab-ci.yml** file. This file essentially determines what our pipeline looks like and what it does. 
Our work here will be divided in two "layers": **stages** and **jobs**.

**Stages** in Gitlab CI are logical separations between promotions of software to an environment, or "phases" in our 
delivery value stream like "build", "test", "deploy" and more. 

These stages contain one or more **jobs**, which are units of work within a stage and are the quality gates for the software. 
This is where we define the actual scripts for the pipeline to run. 
One thing of note is that every **job** in a **stage** is executed at the same time, whereas a stage only starts when 
the previous stage has successfully completed*. Any failures will stop the pipeline completely and immediately**.

### Structure

The [.gitlab-ci.yml](../.gitlab-ci.yml) file is structured in three parts:

- Build settings like defaults, caching settings and variables
- Stages, the build stages of the CI/CD pipeline
- Jobs, the build steps of the CI/CD pipeline

```yaml
# Default docker image
image: node:10

# GitLab CI settings
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
  - node_modules/

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2

before_script:
  - npm install

# Stages

stages:
  - sample


# Jobs

sample:
  stage: sample
  script:
   - echo 'sample'
```

In the following exercises this file will be extended with additional steps to build trust in our change prior to a deployment. 
We'll do so with a by checking the syntax of our sample application first. Continue to exercise 3: [Syntax checking](exercises/syntax-checking.md)! 

----------


*There are exceptions, like the concept of a [Directed Acyclic Graph pipeline](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/) where you can execute jobs "out of order". 

**Again, there are exceptions. You can execute certain steps *only* on failure, or allow certain jobs to fail. This can be handy during experimentation phases. 